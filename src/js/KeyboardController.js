import { Controller } from "jsnes";
export default class KeyboardController {
  constructor(props) {
    this.onButtonDown = props.onButtonDown;
    this.onButtonUp = props.onButtonUp;
  }
  handleKey(e) {
    let key = undefined;
    switch (e.code) {
      case "KeyW":
      case "ArrowUp":
        key = Controller.BUTTON_UP;
        break;
      case "KeyA":
      case "ArrowLeft":
        key = Controller.BUTTON_LEFT;
        break;
      case "KeyS":
      case "ArrowDown":
        key = Controller.BUTTON_DOWN;
        break;
      case "KeyD":
      case "ArrowRight":
        key = Controller.BUTTON_RIGHT;
        break;
      case "KeyC":
        key = Controller.BUTTON_SELECT;
        break;
      case "KeyV":
        key = Controller.BUTTON_START;
        break;
      case "KeyJ":
      case "KeyU":
        key = Controller.BUTTON_B;
        break;
      case "KeyK":
      case "KeyI":
        key = Controller.BUTTON_A;
        break;
      default:
        break;
    }
    return key;
  }
  handleKeyDown = e => {
    let key = this.handleKey(e);
    if (key !== undefined) {
      this.onButtonDown(1, key);
      e.preventDefault();
    }
  }

  handleKeyUp = e => {
    let key = this.handleKey(e);
    if (key !== undefined) {
      this.onButtonUp(1, key);
      e.preventDefault();
    }
  }

  handleKeyPress = e => {
    e.preventDefault();
  }
  start() {
    document.addEventListener("keydown", this.handleKeyDown);
    document.addEventListener("keyup", this.handleKeyUp);
  }
  stop() {
    document.removeEventListener("keydown", this.handleKeyDown);
    document.removeEventListener("keyup", this.handleKeyUp);
  }
}
