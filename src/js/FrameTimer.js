export default class FrameTimer {
  constructor(props) {
    this.onGenerateFrame = props.onGenerateFrame;
    this.onWriteFrame = props.onWriteFrame;
    this.running = false;
    this.i = 0;
  }

  start() {
    if (!this.running) {
      this.running = true;
      this.i = 0;
      this.requestAnimationFrame();
    }
  }

  stop() {
    this.running = false;
    this.i = 0;
    if (this.animationId) window.cancelAnimationFrame(this.animationId);
  }

  requestAnimationFrame() {
    this.animationId = window.requestAnimationFrame(this.onAnimationFrame);
  }

  onAnimationFrame = () => {
    this.requestAnimationFrame();
    this.onGenerateFrame();
    this.onWriteFrame();
  };
}
