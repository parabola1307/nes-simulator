const SCREEN_WIDTH = 256;
const SCREEN_HEIGHT = 240;
export default class Screen {
    constructor(props) {
        this.canvas = props.canvas;
        this.canvas.width = SCREEN_WIDTH;
        this.canvas.height = SCREEN_HEIGHT;
        this.context = this.canvas.getContext("2d");
        this.imageData = this.context.getImageData(
            0,
            0,
            SCREEN_WIDTH,
            SCREEN_HEIGHT
        );

        this.context.fillStyle = "black";
        this.context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        this.buf = new ArrayBuffer(this.imageData.data.length);
        this.buf8 = new Uint8ClampedArray(this.buf);
        this.buf32 = new Uint32Array(this.buf);
        for (var i = 0; i < this.buf32.length; ++i) {
            this.buf32[i] = 0xff000000;
        }
    }
    setBuffer(buffer) {
        var i = 0;
        for (var y = 0; y < SCREEN_HEIGHT; ++y) {
            for (var x = 0; x < SCREEN_WIDTH; ++x) {
                i = y * 256 + x;
                this.buf32[i] = 0xff000000 | buffer[i];
            }
        }
    }
    writeBuffer() {
        this.imageData.data.set(this.buf8);
        this.context.putImageData(this.imageData, 0, 0);
    }
}