import md5 from "md5";
function fileReader(file) {
  return new Promise((resolve) => {
    var reader = new FileReader();
    reader.onload = resolve;
    reader.readAsBinaryString(file);
  });
}

function load() {
  const romsInfo = localStorage.getItem("roms");
  return romsInfo ? JSON.parse(romsInfo) : {}
}
function del(hash) {
  localStorage.removeItem("rom-" + hash);
  const roms = load();
  if (roms[hash]) {
    roms[hash] = undefined;
    localStorage.setItem("roms", JSON.stringify(roms));
  }
}
function get(hash) {
  const roms = load();
  const rom = roms[hash];
  if (rom) {
    const byteString = localStorage.getItem("rom-" + hash);
    if (byteString) {
      rom["rom"] = byteString;
      return rom;
    }
  }
}
async function save(file) {
  const readFile = await fileReader(file);
  const byteString = readFile.target.result;
  const hash = md5(byteString);
  const rom = {
    name: file.name.substring(0, file.name.lastIndexOf("."))
  };
  const roms = load();
  roms[hash] = rom;
  localStorage.setItem("roms", JSON.stringify(roms));
  localStorage.setItem("rom-" + hash, byteString);
  return hash;
}
export default {
  save,
  del,
  load,
  get,
};
